"use client";

import Banner from "@/components/Banner";
import BaseLayout from "@/components/BaseLayout";

function Page() {
  return (
    <BaseLayout>
      <Banner title="Careers" summary="This is careers page" />
    </BaseLayout>
  );
}

export default Page;
