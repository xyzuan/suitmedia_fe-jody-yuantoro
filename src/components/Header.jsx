"use client";

import React, { useEffect, useState } from "react";
import Link from "next/link";
import { routes } from "@/constant/routes";

function Header() {
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [visible, setVisible] = useState(true);
  const [currentPath, setCurrentPath] = useState("/");

  const handleScroll = () => {
    const currentScrollPos = window.pageYOffset;

    setVisible(
      (prevScrollPos > currentScrollPos &&
        prevScrollPos - currentScrollPos > 70) ||
        currentScrollPos < 10
    );
    setPrevScrollPos(currentScrollPos);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    setCurrentPath(window.location.pathname);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [prevScrollPos, visible]);

  return (
    <nav
      className={`fixed w-full z-20 top-0 left-0 transition-all duration-300 ${
        visible ? "bg-[#FC4F08]" : "bg-[#FC4F08] bg-opacity-60 backdrop-blur-xl"
      }`}
    >
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4 px-[24px]">
        <Link href="/">
          <img
            width={125}
            height={50}
            alt="suitmedia"
            src="/assets/images/suitmedia_white.png"
          />
        </Link>

        <div className="flex md:order-2">
          <div className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1">
            <ul className="flex flex-col p-4 md:p-0 mt-4 md:flex-row md:space-x-8">
              {routes.map((item, index) => (
                <li key={index}>
                  <Link href={item.path} className="text-white">
                    {item.label}
                  </Link>
                  {currentPath === item.path && (
                    <div className="w-full h-[4px] mt-3 bg-white rounded-full" />
                  )}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
