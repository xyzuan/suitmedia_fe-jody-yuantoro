"use client";

import Banner from "@/components/Banner";
import BaseLayout from "@/components/BaseLayout";

function Page() {
  return (
    <BaseLayout>
      <Banner title="Contact" summary="This is contact page" />
    </BaseLayout>
  );
}

export default Page;
