"use client";

import Banner from "@/components/Banner";
import BaseLayout from "@/components/BaseLayout";

function Page() {
  return (
    <BaseLayout>
      <Banner title="Works" summary="This is works page" />
    </BaseLayout>
  );
}

export default Page;
