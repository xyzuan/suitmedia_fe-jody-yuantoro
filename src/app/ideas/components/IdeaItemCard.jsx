import { formatDate } from "@/utils/dateFormatter";
import {
  Card,
  CardActionArea,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import React from "react";

function IdeaItemCard({ i, v }) {
  return (
    <Grid
      key={i}
      item
      xs={12}
      sm={6}
      md={3}
      style={{ display: "flex", flexDirection: "column" }}
    >
      <Card
        sx={{
          display: "flex",
          height: "100%",
          borderRadius: "24px",
          marginX: 2,
          marginY: 2,
          boxShadow: "rgba(0, 0, 0, 0.25) 0px 25px 50px -12px",
        }}
      >
        <CardActionArea
          sx={{
            height: "100%",
            width: "100%",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div className="h-[144px] w-full overflow-hidden rounded-t-xl flex-shrink-0">
            <img
              className="w-full h-full object-cover"
              src={v.medium_image[0]?.url}
              loading="lazy"
            />
          </div>
          <CardContent
            sx={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              flexGrow: 1,
            }}
          >
            <Typography gutterBottom component="div" className="text-gray-400">
              {formatDate(v.published_at)}
            </Typography>
            <h1 className="line-clamp-3 font-bold text-xl">{v.title}</h1>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}

export default IdeaItemCard;
