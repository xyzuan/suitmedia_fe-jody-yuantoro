"use client";

import axios from "axios";
import {
  Box,
  Container,
  Grid,
  MenuItem,
  Pagination,
  Select,
  Skeleton,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import { BASE_API_URL } from "@/constant/api";
import { generateParamFilter } from "@/redux/apiHelper";
import Banner from "@/components/Banner";
import IdeaItemCard from "./components/IdeaItemCard";

function Page() {
  const cookies = new Cookies();

  const getCookieValue = (cookieName, defaultValue) => {
    const cookieValue = cookies.get(cookieName);
    return cookieValue !== undefined ? cookieValue : defaultValue;
  };

  const [ideasData, setIdeasData] = useState(null);
  const [ideasLoading, setIdeasLoading] = useState(true);
  const [ideasFetching, setIdeasFetching] = useState(false);

  const [page, setPage] = useState(() => parseInt(getCookieValue("page", "1")));
  const [pageSize, setPageSize] = useState(() =>
    parseInt(getCookieValue("pageSize", "10"))
  );
  const [sort, setSort] = useState(() =>
    getCookieValue("sort", "-published_at")
  );

  useEffect(() => {
    cookies.set("page", page.toString());
    cookies.set("pageSize", pageSize.toString());
    cookies.set("sort", sort);
  }, [page, pageSize, sort]);

  const params = {
    pageSize,
    page,
    sort,
    append: "medium_image",
  };

  const fetchIdeas = async () => {
    try {
      setIdeasFetching(true);
      const response = await axios.get(
        `${BASE_API_URL}/ideas?${generateParamFilter(params)}`
      );

      setIdeasData(response.data);
      setIdeasLoading(false);
      setIdeasFetching(false);
    } catch (error) {
      console.error("Error fetching ideas:", error);
      setIdeasLoading(false);
      setIdeasFetching(false);
    }
  };

  useEffect(() => {
    fetchIdeas(pageSize, page, sort);
  }, [pageSize, page, sort]);

  const handlePageChange = (event, value) => {
    setPage(value);
  };

  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
  };

  const handleSortChange = (event) => {
    setSort(event.target.value);
  };

  return (
    <>
      <Banner title="Ideas" summary="Where all our great things begin" />
      <Container
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            width: "100%",
            marginY: "4rem",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography>
            Showing {page} - {pageSize}
          </Typography>
          <Box
            gap={3}
            sx={{
              display: "flex",
            }}
          >
            <Box
              gap={2}
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Show per page: </Typography>
              <Select
                value={pageSize}
                onChange={handlePageSizeChange}
                sx={{
                  borderRadius: "99px",
                  width: "110px",
                }}
              >
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={20}>20</MenuItem>
                <MenuItem value={50}>50</MenuItem>
              </Select>
            </Box>
            <Box
              gap={2}
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Sort by: </Typography>
              <Select
                value={sort}
                onChange={handleSortChange}
                sx={{
                  borderRadius: "99px",
                  width: "110px",
                }}
              >
                <MenuItem value="-published_at">Newest</MenuItem>
                <MenuItem value="published_at">Oldest</MenuItem>
              </Select>
            </Box>
          </Box>
        </Box>
        {!ideasLoading && !ideasFetching ? (
          <Grid container>
            {ideasData?.data?.map((v, i) => {
              return <IdeaItemCard i={i} v={v} />;
            })}
          </Grid>
        ) : (
          <Grid container>
            <Grid item xs={6} sm={6} md={3} lg={3}>
              <Skeleton
                sx={{
                  width: "100%",
                  height: "350px",
                  borderRadius: "24px",
                  marginX: 2,
                  marginY: 2,
                  boxShadow: "rgba(0, 0, 0, 0.25) 0px 25px 50px -12px",
                }}
              />
            </Grid>
          </Grid>
        )}
        <Pagination
          className="my-16"
          count={10}
          page={page}
          onChange={handlePageChange}
          color="primary"
          shape="rounded"
        />
      </Container>
    </>
  );
}

export default Page;
