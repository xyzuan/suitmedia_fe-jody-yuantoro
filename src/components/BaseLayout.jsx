import React, { ReactNode } from "react";
import Header from "./Header";
import { ThemeProvider } from "@mui/material";
import { Provider } from "react-redux";
import { theme } from "@/constant/theme";
import { store } from "@/redux/store";

const BaseLayout = ({ children }) => {
  return (
    <div suppressHydrationWarning className="overflow-hidden">
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Header />
          <main>{children}</main>
        </Provider>
      </ThemeProvider>
    </div>
  );
};

export default BaseLayout;
