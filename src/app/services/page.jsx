"use client";

import Banner from "@/components/Banner";
import BaseLayout from "@/components/BaseLayout";

function Page() {
  return (
    <BaseLayout>
      <Banner title="Services" summary="This is services page" />
    </BaseLayout>
  );
}

export default Page;
