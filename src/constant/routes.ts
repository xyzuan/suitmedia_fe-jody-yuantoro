export const routes = [
  { path: "/works", label: "Work" },
  { path: "/about", label: "About" },
  { path: "/services", label: "Services" },
  { path: "/", label: "Ideas" },
  { path: "/careers", label: "Careers" },
  { path: "/contact", label: "Contact" },
];
