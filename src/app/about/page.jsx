"use client";

import Banner from "@/components/Banner";
import BaseLayout from "@/components/BaseLayout";

function Page() {
  return (
    <BaseLayout>
      <Banner title="About" summary="This is about page" />
    </BaseLayout>
  );
}

export default Page;
