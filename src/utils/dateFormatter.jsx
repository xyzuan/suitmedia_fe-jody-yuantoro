import { format, parseISO } from "date-fns";
import { id } from "date-fns/locale";

export const formatDate = (dateString) => {
  const parsedDate = parseISO(dateString);
  return format(parsedDate, "d MMMM yyyy", { locale: id });
};
