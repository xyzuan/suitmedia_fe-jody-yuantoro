import React from "react";
import { Background, Parallax } from "react-parallax";

export const bannersMock = [
  {
    id: 1,
    mime: "image/jpeg",
    file_name: "6.jpg",
    url: "https://suitmedia.static-assets.id/storage/files/601/6.jpg",
  },
];

function Banner({ title, summary }) {
  return (
    <Parallax strength={200} bgImage={bannersMock[0].url} className="w-screen">
      <div className="relative px-4 py-24 mx-auto text-center lg:py-56">
        <h1 className="text-white text-6xl">{title}</h1>
        <p className="text-white">{summary}</p>
      </div>
      <div className="w-0 h-0 border-r-[100vw] border-r-transparent border-b-[13vw] border-white mt-[-5vw] transform scale-x-[-1] "></div>
    </Parallax>
  );
}

export default Banner;
