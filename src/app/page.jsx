"use client";

import BaseLayout from "@/components/BaseLayout";
import Page from "./ideas/page";

export default function Home() {
  return (
    <BaseLayout>
      <Page />
    </BaseLayout>
  );
}
